//
//  ViewController.swift
//  timer
//
//  Created by  drake on 2020/04/11.
//  Copyright © 2020  drake. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelMin: UILabel!
    @IBOutlet weak var labelSec: UILabel!
    @IBOutlet weak var labelMilSec: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var reSetButton: UIButton!
    
    var isStart = false
    var timer = Timer()
    var startTime = 0.0
    var elapsed = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        startStopButton.setTitle("Start", for: .normal)
        reSetButton.setTitle("Reset", for: .normal)
        labelMin.text = "00"
        labelSec.text = "00"
        labelMilSec.text = "00"
    }

    @IBAction func StartButtonClick(_ sender: Any) {
        if isStart {
            timer.invalidate()
            startStopButton.setTitle ("start", for: .normal)
        }
        else {
            startStopButton.setTitle("stop", for: .normal)
            startTime = Date().timeIntervalSince1970 - elapsed
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        }
        isStart = !isStart
        
        printelapsed()
    }
    
    func printelapsed(){
        elapsed = Date().timeIntervalSince1970 - startTime
        let date = Date(timeIntervalSince1970: elapsed)
        let df = DateFormatter()
        df.dateFormat = "mm"
        labelMin.text = df.string(from: date)
        df.dateFormat = "ss"
        labelSec.text = df.string(from: date)
        df.dateFormat = "SS"
        labelMilSec.text = df.string(from: date)
    }
    
    @IBAction func ResetButtonClick(_ sender: Any) {
        elapsed = 0.0
        startTime = Date().timeIntervalSince1970
        printelapsed()
    }
    
    @objc func updateCounter() {
        printelapsed()
    }
}
